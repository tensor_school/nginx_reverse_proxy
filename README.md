# Nginx reverse proxy

Контейнер в состве Docker Compose обеспечивающий https шифрование, одновременный доступ к API серверу и Nginx со статическими файлами.

**HTTPS**:

certbot команда:
```
sudo docker run -it --rm --name certbot -v "/etc/letsencrypt:/etc/letsencrypt" -v "/var/lib/letsencrypt:/var/lib/letsencrypt" -p 80:80 certbot/certbot certonly -d <example.com>
```
Пути к сертификатам: `/var/lib/letsencrypt`, `/etc/letsencrypt`
